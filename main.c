#include "unity.h"
#include "estatistica.h"

// Função auxiliar utilizada pela biblioteca unity.
void setUp() {
  return;
}

// Função auxiliar utilizada pela biblioteca unity.
void tearDown() {
  return;
}
// Teste da média com sequência positiva
void test_positivo_media(){
  double v[] = {1, 2, 3};
  printf("** Teste positivo da média... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(2, media(3, v));
}
// Teste com sequência cujos todos os elementos são iguais
void test_mesmo_elemento_media(){
  double v[] = {10, 10, 10, 10, 10};
  printf("** Teste média como elementos iguais... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(10, media(5, v));
}
// Teste de média com sequência negativa
void test_negativo_media(){
  double v[] = {-1, -10, -4};
  printf("** Teste da média com elementos negativos... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(-5, media(3, v));
}
//Teste da média com sequência de valores alternados
void test_alt_media(){
  double v[] = {1, -10, 1.5};
  printf("** Teste da média com elementos de sinais alternados... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(-2.5, media(3, v));
}
// Teste da média para sequência com um elemento
void test_unico_elemento_media(){
  double v[] = {2.3};
  printf("** Teste da média com sequência de um elemento... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(2.3, media(1, v));
}
// Teste da função média para uma sequência nula.
void test_nulo_media(){
  double v[] = {0, 0, 0, 0, 0, 0, 0};
  printf("** Teste da média com sequência nula... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(0, media(1, v));
}

// O desvio padrão de v é 1, então o teste serve para verificar a condição.
void test_positivo_desvio() {
  double v[] = {1, 2, 3};  
  printf("** Teste positivo do desvio padrão... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(1, desvio(3, v));
}

// O desvio padrão de v é 1, então o teste serve para verificar a condição.
void test_alt_desvio() {
  double v[] = {100, -20, 30, -40, 10, 40, -50};
  printf("** Teste do desvio padrão vetor com mais elementos... **\n");
  printf("%lf\n", desvio(7, v));
  // Como valor de retorno tem muita casas decimais, então foi colocado um limite inferior e superior. O valor esperado é 52.281290.
  TEST_ASSERT_TRUE(desvio(7, v) > 52.28 && desvio(7, v) < 52.29);
}

// O desvio padrão da sequência é 1, então será realizado o teste de falsidade.
void test_negativo_desvio() {
    double v[] = {-1, -2, -3};
    printf("** Teste falso do desvio padrão... **\n");
    TEST_ASSERT_EQUAL_DOUBLE(1, desvio(3, v));
}

//  O desvio padrão de uma sequência com um elemento é 0.
void test_elemento_unico_desvio() {
  double v[] = {2};
  printf("** Teste do desvio padrão de vetor com elemento único... **\n");
  TEST_ASSERT_FALSE(desvio(1, v) == 2);
  TEST_ASSERT_EQUAL_DOUBLE(0, desvio(1, v));
}

// O desvio padrão de sequência com elementos iguais deve ser 0.
void test_mesmo_valor_desvio(){
  double v[] = {100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
  printf("** Teste do desvio padrão em vetor com elementos iguais... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(0, desvio(12, v));
}

// O desvio padrão de sequência com elementos iguais deve ser 0.
void test_nulo_desvio(){
  double v[] = {0, 0, 0, 0, 0, 0, 0, 0};
  printf("** Teste do desvio padrão em vetor nulo... **\n");
  TEST_ASSERT_EQUAL_DOUBLE(0, desvio(8, v));
}

int main() {  
  UNITY_BEGIN();
  RUN_TEST(test_positivo_media);
  RUN_TEST(test_negativo_media);
  RUN_TEST(test_mesmo_elemento_media);
  RUN_TEST(test_alt_media);
  RUN_TEST(test_unico_elemento_media);
  RUN_TEST(test_nulo_media);
  printf("\n");
  RUN_TEST(test_positivo_desvio);
  RUN_TEST(test_alt_desvio);
  RUN_TEST(test_negativo_desvio);
  RUN_TEST(test_elemento_unico_desvio);
  RUN_TEST(test_mesmo_valor_desvio);
  RUN_TEST(test_nulo_desvio);
  return UNITY_END();
}
