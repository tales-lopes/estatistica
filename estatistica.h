#ifndef ESTATISTICA_H
#define ESTATISTICA_H

double soma(int n, double * v);
double media(int n, double * v);
double desvio(int n, double *v);

#endif