#include "estatistica.h"
#include <math.h>
#include <stdio.h>

// Argumentos: n é a quantidade de elementos do vetor, v é o vetor de elementos;
// A função soma iterará sobre o vetor v de tamanho n e retornará a soma dos elementos.

double soma(int n, double * v){
    
    // Variáveis para iteração (int i) e armazenamento de soma (double soma)
    int i;
    double soma = 0;

    for (i = 0; i < n; i++)
        soma += v[i];
    
    return soma;
}

// Argumentos: n é o tamanho do vetor v, v é o vetor de elementos;
// A função retornará a média dos valores dos elementos de v, utilizando a função soma().

double media(int n, double * v){
    
    double media;
    media = soma(n, v) / n;

    return media;
}

// Argumentos: n é o tamanho do vetor v, v é o vetor de elementos;
// A função retornará o desvio padrão dos n elementos de v. A função media() é utilizada para auxiliar o cálculo.

double desvio(int n, double *v){
    double mean = 0;
    double difference = 0;
    double sum = 0;
    double radicando = 0;
    double desvio_padrao = 0;

    if(n <= 1){
        return 0;
    }

    mean = media(n, v);

    for(int i = 0; i < n; i++){
        difference = v[i] - mean;
        sum += pow(difference, 2);
    }

    radicando = sum / (n - 1);
    desvio_padrao = sqrt(radicando);
    return desvio_padrao;
}