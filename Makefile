estatistica: main.o estatistica.o unity.o
	gcc main.o estatistica.o unity.o -lm -o estatistica

main.o: main.c estatistica.h unity.h unity_internals.h
	gcc -c main.c

estatistica.o: estatistica.c estatistica.h
	gcc -c estatistica.c

unity.o: unity.c unity.h unity_internals.h
	gcc -c unity.c

clean:
	rm -f *~ estatistica *.o
